Name:           compton
Version:        0
Release:        0.4.20150419gb1889c1%{?dist}
Summary:        Compositor for X

License:        MIT
URL:            https://github.com/chjj/%{name}

# The source for this package was pulled from upstream's vcs.  Use the
# following command to generate the tarball:
# wget -O chjj-compton-gb1889c1.tar.gz --no-check-certificate --content-disposition http://github.com/chjj/compton/tarball/b1889c1

Source0:        chjj-compton-gb1889c1.tar.gz

BuildRequires:	asciidoc
BuildRequires:	dbus-devel
BuildRequires:	libconfig-devel
BuildRequires:	libdrm-devel
BuildRequires:	libGL-devel
BuildRequires:	libXrandr-devel
BuildRequires:	libXdamage-devel
BuildRequires:	libXcomposite-devel
BuildRequires:	libXinerama-devel
BuildRequires:	pcre-devel
BuildRequires:	pkgconfig

Requires:       xorg-x11-utils

%description
Compton is a compositor for X, and a fork of xcompmgr-dana.

%prep
%setup -q -n chjj-compton-b1889c1


%build
CFLAGS="%{optflags}" make %{?_smp_mflags}
make docs

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT


%files
%doc LICENSE README.md
%{_bindir}/%{name}
%{_bindir}/%{name}-trans
%{_mandir}/man1/%{name}.1.*
%{_mandir}/man1/%{name}-trans.1.*
%{_datadir}/applications/%{name}.desktop


%changelog
* Mon Jun 22 2015 Eduardo Ito <itoed@yahoo.com> - 0-0.4.20150419gb1889c1
- New Git snapshot
- Require libXinerama-devel and dbus-devel for build
- Add desktop to files section

* Wed Feb 13 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0-0.3.20121218g75aec17
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild


* Sun Oct 07 2012 Mario Blättermann <mariobl@fedoraproject.org> - 0-0.2.20121218g75aec17
- New Git snapshot
- Changed license to MIT, according to the developer's declaration
- Add "make" call to produce newer manpages
- Some BR cleanup

* Sun Oct 07 2012 Mario Blättermann <mariobl@fedoraproject.org> - 0.1.20121007gitc7ca345
- New Git snapshot
- settrans has been renamed to compton-trans

* Wed Jul 18 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0-0.2.20120603gitd52f7a0
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild


* Tue Jul 03 2012 Mario Blättermann <mariobl@fedoraproject.org> - 0.1.20120603gitd52f7a0
- Removed gz extension from the manpage
- Added optflags macro to fill the debug package with real content
- Use version number 0 because there wasn't any release yet
- Added runtime requirement for settrans
- Changed license to BSD

* Thu Jun 28 2012 Mario Blättermann <mariobl@fedoraproject.org> - 0.1-20120603gitd52f7a0
- Initial package
