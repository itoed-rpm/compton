FROM fedora:22

RUN dnf -y install rpm-build make gcc asciidoc dbus-devel libconfig-devel \
	libdrm-devel libGL-devel libXrandr-devel libXdamage-devel \
	libXcomposite-devel libXinerama-devel pcre-devel pkgconfig wget ; \
	dnf clean all
