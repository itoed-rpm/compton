# compton RPM Package

This is an RPM package for compton.

Docker and docker-dompose may be used to build the rpm:

	docker-compose run --rm rpmbuild

## Copying

The compton software is distributed under the MIT license.

This packaging project is distrubuted under the ISC license.
